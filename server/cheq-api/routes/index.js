var express = require('express');
var router = express.Router();
const mysql = require('mysql');
const joi = require('joi');

// Create a connection to mysql
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'Naor_4135',
    database: 'cheq_exam'
});

connection.connect(err => {
    if(err) {
        return err;
    }
});

// Define JSON schema
const schema = joi.object().keys({
    vastURL: joi.string().max(600).required(),
    position: joi.string(),
    hideUI: joi.number().integer().min(0).max(1)
});

// GET `/fetch_vast?id=INT`
// This API will retrieve a VAST XML
router.get('/fetch_vast', function(req, res, next){
    const { id } = req.query;
    
    // get from mysql by id
    const FIND_BY_ID_QUERY = `SELECT * FROM vasts WHERE id=${id}`;

    connection.query(FIND_BY_ID_QUERY, (err, results) => {
        if(err) {
            return res.send(err);
        } else {
            if (results.length == 0 ){
                let default_template = `<VAST version="2.0"></VAST>`;
                res.type('application/xml');
                res.send(default_template);
            } else {
                const { id, vast_url, position, hide_ui } = results[0];
                let xml_layout = 
                    `<VAST version="2.0">
                        <Ad id="ComboGuard">
                            <InLine>
                                <AdSystem>2.0</AdSystem>
                                <Impression/>
                                <Creatives>
                                    <Creative>
                                        <Linear>
                                        <Duration>00:00:19</Duration>
                                        <MediaFiles>
                                            <MediaFile type="application/x-shockwave-flash" apiFramework="VPAID" height="168" width="298" delivery="progressive">
                                                <![CDATA[http://localhost/ComboWrapper.swf?vast=${vast_url}&position=${position}&hideUI=${hide_ui}&videoId=${id}]]>
                                            </MediaFile>
                                            <MediaFile type="application/javascript" apiFramework="VPAID" height="168" width="298" delivery="progressive">
                                                <![CDATA[http://localhost/ComboWrapper.js?vast=${vast_url}&position=${position}&hideUI=${hide_ui}&videoId=${id}]]>
                                            </MediaFile>
                                        </MediaFiles>
                                        </Linear>
                                    </Creative>
                                </Creatives>
                            </InLine>
                        </Ad>
                    </VAST>`;

                res.type('application/xml');
                res.send(xml_layout);
            }
        }
    });
});

// POST `create_vast`
// Route for creating new Vast
router.post('/create_vast', function(req, res, next){
    let { vastURL, position, hideUI } = req.body;

    position = (position === undefined) ? 'bottom_right' : position; 
    hideUI = (hideUI === undefined) ? 0 : hideUI;
    const CREATE_NEW_VAST_QUERY = `INSERT INTO vasts (vast_url, position, hide_ui) VALUES('${vastURL}', '${position}', '${hideUI}')`;

    // validate the input according to schema before executing the query
    const result = joi.validate(req.body, schema, (err, value) => {
        if(err) {
            return res.send(err);
        } else {
            // create new Vast in the DB
            connection.query(CREATE_NEW_VAST_QUERY, (err, results) => {
                if(err) {
                    return res.send(err);
                } else {
                    return res.send('successfuly added vast');
                }
            });
        }
    });
});

module.exports = router;