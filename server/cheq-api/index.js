const express = require('express');
const app = express();
const routes = require('./routes');

const cors = require('cors');
const mysql = require('mysql');
const jsonParser = require('body-parser').json;
const logger = require('morgan');

app.use(cors());
app.use(logger('dev'));
app.use(jsonParser());

app.use('/', routes);

// Catch 404 and forward to error handler
app.use((req, res, next) => {
    let err = new Error("Not Found");
    err.status = 404;
    next(err);
});

// Error Handler
app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.json({
        error: {
            message: err.message
        }
    });
});

app.listen(4000, () => {
	console.log('The application is running on localhost:4000!');
});