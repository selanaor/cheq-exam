import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';
import logo from './logo.svg';
import './App.css';

let NewVastForm = props => {
  const { handleSubmit } = props;
  return <form onSubmit={handleSubmit} className="form">
    <div className="field">
      <div className="control">
        <label className="label">Vast URL</label>
        <Field className="input" name="vastURL" component="input" type="text" placeholder="Vast URL"/>
      </div>
    </div>

    <div className="field">
      <div className="control">
        <label className="label">Position</label>
        <div className="select">
          <Field className="input" name="position" component="select" placeholder="bottom_right">
            <option value="top_left">top_left</option>
            <option value="top_middle">top_middle</option>
            <option value="top_right">top_right</option>
            <option value="middle_left">middle_left</option>
            <option value="middle_right">middle_right</option>
            <option value="bottom_left">bottom_left</option>
            <option value="bottom_middle">bottom_middle</option>
            <option value="bottom_right">bottom_right</option>
          </Field>
        </div>
      </div>
    </div>

    <div className="field">
      <div className="control">
        <label className="label">Hide UI</label>
        <Field className="input" name="hideUI" component="input" type="number" min="0" max="1"/>
      </div>
    </div>

    <div className="field">
      <div className="control">
        <button className="button is-link">Submit</button>
      </div>
    </div>

  </form>;
};

NewVastForm = reduxForm({
  form: 'newVast',
})(NewVastForm);


class App extends Component {

  createNewVast = values => {
      const { vastURL, position, hideUI } = values;
      
      fetch('http://localhost:4000/create_vast/', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            vastURL: vastURL,
            position: position,
            hideUI: hideUI
          })
        });
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to CheQ API</h1>
        </header>
        <div className="container">
          <p className="App-intro">
            Create a new Vast
          </p>
          <NewVastForm onSubmit={this.createNewVast} />
        </div>
      </div>
    );
  }
}

export default App;
