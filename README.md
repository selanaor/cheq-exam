# CHEQ Exam

- Go to 'cheq-exam/server/cheq-api'
- run 'npm install'
- run 'SET DEBUG=cheq-api:* & npm run devstart' to start the server, it will run on http://localhost:4000

- Go to 'cheq-exam/client/cheq-client'
- run 'npm install'
- run 'npm start' the application will run on http://localhost:3000


